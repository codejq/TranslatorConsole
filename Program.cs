﻿using System.IO;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Text;
using Saycotec.gTranslateConsole.Services;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Saycotec.gTranslateConsole.Dto.Settings.Translate;
using System.Text.Encodings.Web;
using System.Text.Unicode;

ConcurrentDictionary<string, string> dictionaryLanguages = new ConcurrentDictionary<string, string>();
bool escKey = false;

/**
    Create a console translator where can translate any text using Google Translator API.
    It program should allow select the source and target language. These configurations will save in a json configuration file,
    and these configuration will load to start this program by default.
       
**/

Console.WriteLine("*****************************************************************");
Console.WriteLine("******************* CONSOLE GOOGLE TRANSLATOR *******************");
Console.WriteLine("*****************************************************************");
Console.WriteLine("*****************************************************************");

const string prefixSpace = "   ";

// Press escape key to exit otherwise use the translator.
do
{
    Console.WriteLine("_________________________________________________________________");
    Console.WriteLine("Current Translation:\t {1}({0}) --> {3}({2})", ConfigurationManager.getCurrentSourceCode(), ConfigurationManager.getCurrentSourceName(), ConfigurationManager.getCurrentTargetCode(), ConfigurationManager.getCurrentTargetName());
    Console.WriteLine("-----------------------------------------------------------------");
    Console.WriteLine("{0}Press Ctrl+S to switch languages.", prefixSpace);
    Console.WriteLine("{0}Press any key to translate.", prefixSpace);
    // Console.WriteLine("/////////////////////////////////////////////////////////////////");
    Console.WriteLine("-----------------------------------------------------------------");
    Console.WriteLine("Press Ctrl+X to configure other languages.");
    Console.WriteLine("Press ESC key to exit.");
    Console.WriteLine();

    /*
    Console.WriteLine("App Name : {0} ", ConfigurationManager.getAppName());
    Console.WriteLine("Json Name Config: {0}", ConfigurationManager.getDefaultJsonNameConfig());
    Console.WriteLine("Source Language : {0} ", ConfigurationManager.getSourceLanguage());
    Console.WriteLine("Target Language : {0} ", ConfigurationManager.getTargetLanguage());
    */

    ConsoleKeyInfo consoleKeyInfo = Console.ReadKey(true);

    // Get current languages setting.
    string? sourceLanguageCode = ConfigurationManager.getCurrentSourceCode();
    string? targetLanguageCode = ConfigurationManager.getCurrentTargetCode();
    string? sourceLanguageName = ConfigurationManager.getCurrentSourceName();
    string? targetLanguageName = ConfigurationManager.getCurrentTargetName();

    // When press Escape key to exit program.
    if (consoleKeyInfo.Key == ConsoleKey.Escape)
    {
        escKey = true;
        break;
    }
    // Switch the source to target language
    else if (consoleKeyInfo.Modifiers != 0 && consoleKeyInfo.Key == ConsoleKey.S)
    {
        ConfigurationManager.changeCurrentLanguage(ConfigurationManager.getSourceText(), targetLanguageCode, targetLanguageName);
        ConfigurationManager.changeCurrentLanguage(ConfigurationManager.getTargetText(), sourceLanguageCode, sourceLanguageName);
        Console.WriteLine("Configuration saved successfully.");
        continue;
    } 
    // Change language setting, selecting source or target language.
    else if (consoleKeyInfo.Modifiers != 0 && consoleKeyInfo.Key == ConsoleKey.X) 
    {
        Console.WriteLine("Press ctrl + s and select the source language.");
        Console.WriteLine("Press ctrl + t and select the target language.");

        bool pressAllowedKeyConfig = false;

        do
        {
            consoleKeyInfo = Console.ReadKey(true);
            Console.WriteLine();
            Console.WriteLine("List of languages available for translate (Code - Name of country)");

            bool pressCtrlS = consoleKeyInfo.Modifiers != 0 && consoleKeyInfo.Key == ConsoleKey.S;
            bool pressCtrlT = consoleKeyInfo.Modifiers != 0 && consoleKeyInfo.Key == ConsoleKey.T;

            // Change the source language setting.
            if (pressCtrlS)
            {
                getListOfLanguages(ConfigurationManager.getSourceText());
                pressAllowedKeyConfig = true;
            }
            // Change the source language setting.
            else if (pressCtrlT)
            {
                getListOfLanguages(ConfigurationManager.getTargetText());
                pressAllowedKeyConfig = true;
            }
            else
            {
                Console.WriteLine("Invalid command, try again.");
            }

        } while (!pressAllowedKeyConfig);

    }
    // Translate input text.
    else
    {
        bool exitTranslation = false;
        Console.WriteLine("-----------------------------------------------------------------");

        do
        {
            Console.WriteLine("\t\t\t{0} -> {1}", sourceLanguageName, targetLanguageName);
            Console.WriteLine("Type text to translate:", sourceLanguageCode);
            Console.WriteLine("-----------------------");
            string? sourceText = Console.ReadLine();
            sourceText = !string.IsNullOrEmpty(sourceText) ? sourceText.Trim() : string.Empty;

            if (sourceText.Length <= 0)
            {
                Console.WriteLine();
                exitTranslation = true;
                break;
            }
            else
            {
                string jsonResult = await ClientAPITranslate.translateText(sourceText, sourceLanguageCode, targetLanguageCode);
                DtoRespTranslation responseTranslation = JsonSerializer.Deserialize<DtoRespTranslation>(jsonResult);
                if(responseTranslation != null && responseTranslation.sentences != null)
                    Console.WriteLine("=> {1}", targetLanguageCode, responseTranslation.sentences[0].trans);
                else
                    Console.WriteLine("=> {0}", "Invalid translation.");
            }

            Console.WriteLine();
        } while (!exitTranslation);
    }

} while (!escKey);

Console.WriteLine("Program finished.");

/// <summary>
// Código ISO-639 
// https://en.wikipedia.org/wiki/ISO_639
/// </summary>
void getListOfLanguages(string? typeLanguage)
{

    const int LEN_RPAD_COL1 = 15;
    const int LEN_RPAD_COL2 = 50;
    string headerLine = string.Concat("Code".PadRight(LEN_RPAD_COL1), "Country Name".PadRight(LEN_RPAD_COL2));
    headerLine += headerLine;
    Console.WriteLine(headerLine);

    headerLine = string.Concat("...........".PadRight(LEN_RPAD_COL1), ".................................".PadRight(LEN_RPAD_COL2));
    headerLine += string.Concat("...........".PadRight(LEN_RPAD_COL1), ".................................".PadRight(LEN_RPAD_COL2));
    Console.WriteLine(headerLine);

    string pathCsv = "./files/TranslatorLanguages.csv";
    string[] lines = File.ReadAllLines(pathCsv);
    const int COUNT_LANGUAGES_BY_LINE = 2;

    string lineLanguages = string.Empty;

    for (int numLine = 1, columnLine = 1; numLine < lines.Count(); numLine++, columnLine++)
    {
        string[] columns = lines[numLine].Split(";");
        string codeLanguage = columns[1];
        string nameLanguage = columns[0];

        if (columnLine <= COUNT_LANGUAGES_BY_LINE)
        {
            lineLanguages += string.Concat(codeLanguage.PadRight(LEN_RPAD_COL1), nameLanguage.PadRight(LEN_RPAD_COL2));
        }
        else
        {
            Console.WriteLine(lineLanguages);
            columnLine = 1;
            lineLanguages = string.Concat(codeLanguage.PadRight(LEN_RPAD_COL1), nameLanguage.PadRight(LEN_RPAD_COL2));
        }

        dictionaryLanguages.TryAdd(codeLanguage, nameLanguage);
    }

    bool isValidLanguage = false;

    do
    {
        Console.Write("Set Code of {0} Language: ", typeLanguage);
        string? codeLanguageSelected = Console.ReadLine();
        if (codeLanguageSelected != null && dictionaryLanguages.Keys.Contains(codeLanguageSelected))
        {
            isValidLanguage = true;
            string? nameLanguageSelected = dictionaryLanguages.GetValueOrDefault(codeLanguageSelected);
            Console.WriteLine("New {0} Language Selected: {1} - {2}", typeLanguage, codeLanguageSelected, nameLanguageSelected);
            ConfigurationManager.changeCurrentLanguage(typeLanguage, codeLanguageSelected, nameLanguageSelected);
            Console.WriteLine("Configuration saved successfully.");

        }
        else
        {
            Console.WriteLine("Invalid code {0} try again.\n", codeLanguageSelected);
        }

    } while (!isValidLanguage);

}



