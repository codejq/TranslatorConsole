namespace Saycotec.gTranslateConsole.Services;

public static class ClientAPITranslate
{
    readonly static HttpClient httpClient = new HttpClient();
    const int RESPONSE_TYPE = 1;

    public static async Task<string> translateText(string? text, string? sourceLanguage, string? targetLanguage)
    {
        try 
        {
            // Reference doc: https://wiki.freepascal.org/Using_Google_Translate
            string endpoint = string.Format("https://translate.googleapis.com/translate_a/single?client={0}&sl={1}&tl={2}&dt={3}&q={4}&dj={5}","gtx", sourceLanguage, targetLanguage,"t", text, RESPONSE_TYPE);
            using HttpResponseMessage responseMessage = await httpClient.GetAsync(endpoint);
            responseMessage.EnsureSuccessStatusCode();
            string jsonResponse = await responseMessage.Content.ReadAsStringAsync();
            //Console.WriteLine("Response:" + jsonResponse);
            return jsonResponse;
        }
        catch (Exception e)
        {
            throw;
        }

    }

}