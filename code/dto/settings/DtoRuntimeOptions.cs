namespace Saycotec.gTranslateConsole.Dto.Settings.Translate;

public class DtoRuntimeOptions
{
    public string? tfm { get; set; }
    public DtoFramework? framework { get; set; }

    public string? versionApp { get; set; }

    public DtoLanguages? Languages { get; set; }
}

