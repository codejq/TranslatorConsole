namespace Saycotec.gTranslateConsole.Dto.Settings.Translate;

public class DtoLanguages
{
    public DtoCurrentLanguages Current {get; set;}
    public string? SourceText { get; set; }
    public string? TargetText { get; set; }
}