namespace Saycotec.gTranslateConsole.Dto.Settings.Translate;

public class DtoCurrentLanguages
{
    public string? Source { get; set; }
    public string? Target { get; set; }
    public string? TargetName { get; set; }
    public string? SourceName { get; set; }

}