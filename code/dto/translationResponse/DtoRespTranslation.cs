public class DtoRespTranslation
{
    public DtoSentences[] sentences { get; set; }

    public string src { get; set; }
    public DtoSpell spell { get; set; }
}