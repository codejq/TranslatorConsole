using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration;
using System.Text.Json;
using Saycotec.gTranslateConsole.Dto.Settings.Translate;
using System.Text.Encodings.Web;
using System.Text.Unicode;

public static class ConfigurationManager
{
   
    static string jsonDefaultName = $"{getAppName()}.runtimeconfig.json";
    static string jsonPathFile = getAppSettingFile();

    static IConfigurationRoot _configuration = new ConfigurationBuilder().AddJsonFile(jsonPathFile).Build();

    public static string getAppSettingFile()
    {
        return string.Concat(System.AppDomain.CurrentDomain.BaseDirectory, jsonDefaultName);
    }

    public static string getAppName()
    {
        return System.AppDomain.CurrentDomain.FriendlyName;
    }

    public static string getDefaultJsonNameConfig()
    {
        return jsonDefaultName;
    }

    public static string getSourceText()
    {
        string? parameterValue = _configuration.GetSection("runtimeoptions:languages:sourcetext").Value;
        return parameterValue != null ? parameterValue : string.Empty;
    }

    public static string getTargetText()
    {
        string? parameterValue = _configuration.GetSection("runtimeoptions:languages:targettext").Value;
        return parameterValue != null ? parameterValue : string.Empty;
    }

    public static string? getCurrentSourceCode()
    {
        return _configuration.GetSection("runtimeoptions:languages:current:source").Value;
    }

    public static string? getCurrentTargetCode()
    {
        return _configuration.GetSection("runtimeoptions:languages:current:target").Value;
    }

    public static string? getCurrentTargetName()
    {
        return _configuration.GetSection("runtimeoptions:languages:current:targetname").Value;
    }

    public static string? getCurrentSourceName()
    {
        return _configuration.GetSection("runtimeoptions:languages:current:sourcename").Value;
    }

    public static void changeCurrentLanguage(string typeLanguage, string code, string name)
    {
        string? jsonText = File.ReadAllText(jsonPathFile);

        JsonSerializerOptions options = new JsonSerializerOptions { 
            WriteIndented = true, 
            Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping
        };

        DtoRuntimeConfig jsonData = JsonSerializer.Deserialize<DtoRuntimeConfig>(jsonText, options);

        if (!string.IsNullOrEmpty(typeLanguage))
        {
            if (typeLanguage == getSourceText())
            {
                jsonData.runtimeOptions.Languages.Current.Source = code;
                jsonData.runtimeOptions.Languages.Current.SourceName = name;
            }
            else if (typeLanguage == getTargetText())
            {
                jsonData.runtimeOptions.Languages.Current.Target = code;
                jsonData.runtimeOptions.Languages.Current.TargetName = name;
            }
        }

        byte[] newJsonConfig = JsonSerializer.SerializeToUtf8Bytes(jsonData, options);
        File.WriteAllBytes(jsonPathFile, newJsonConfig);
        _configuration = new ConfigurationBuilder().AddJsonFile(jsonPathFile).Build();
    }

}